package com.example.bizplay.repositoryImp;

import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.data.remote.response.LoginResponse;
import com.example.bizplay.data.remote.response.PostResponse;
import com.example.bizplay.data.remote.service.LoginService;
import com.example.bizplay.data.remote.service.RetrofitService;
import com.example.bizplay.data.repository.LoginRepository;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginImp implements LoginRepository {

    private static LoginImp loginImp;
    private final LoginService loginService;

    public LoginImp() {
      loginService = RetrofitService.createService(LoginService.class);
    }

    public static LoginImp newInstance(){
        if (loginImp ==null){
            loginImp = new LoginImp();
        }
        return loginImp;
    }

    @Override
    public MutableLiveData<PostResponse<LoginResponse>> login(String login) {
        MutableLiveData<PostResponse<LoginResponse>> liveData = new MutableLiveData<>();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType, login);
        loginService.createLogin(requestBody).enqueue(new Callback<PostResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<PostResponse<LoginResponse>> call, Response<PostResponse<LoginResponse>> response) {
                liveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<PostResponse<LoginResponse>> call, Throwable t) {

            }
        });

        return liveData;

    }
}
