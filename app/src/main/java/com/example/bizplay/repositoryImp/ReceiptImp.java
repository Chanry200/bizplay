package com.example.bizplay.repositoryImp;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.data.remote.response.PostResponse;
import com.example.bizplay.data.remote.response.ReceiptResponse;
import com.example.bizplay.data.remote.service.ReceiptService;
import com.example.bizplay.data.remote.service.RetrofitService;
import com.example.bizplay.data.repository.ReceiptRepository;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptImp implements ReceiptRepository {

    private static ReceiptImp receiptImp;
    private final ReceiptService receiptService;
    public ReceiptImp(){
        receiptService = RetrofitService.createService(ReceiptService.class);
    }

    public static ReceiptImp newInstance(){
        if (receiptImp == null){
            receiptImp = new ReceiptImp();
        }
        return receiptImp;
    }

    @Override
    public MutableLiveData<PostResponse<ReceiptResponse>> getReceipt (String receipt) {
        MutableLiveData<PostResponse<ReceiptResponse>> liveData = new MutableLiveData<>();

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType,receipt);

        receiptService.getReceipt(requestBody).enqueue(new Callback<PostResponse<ReceiptResponse>>() {
            @Override
            public void onResponse(Call<PostResponse<ReceiptResponse>> call, Response<PostResponse<ReceiptResponse>> response) {
                liveData.setValue(response.body());
                Log.d("chanry", "onResponse: Receipt ");
            }

            @Override
            public void onFailure(Call<PostResponse<ReceiptResponse>> call, Throwable t) {
                Log.d("chanry", "onFailure: "+ t.getMessage());

            }
        });
        return liveData;
    }
}
