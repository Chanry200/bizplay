package com.example.bizplay.repositoryImp;

import static android.content.ContentValues.TAG;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.data.remote.service.MGService;
import com.example.bizplay.data.remote.service.RetrofitService;
import com.example.bizplay.data.repository.MGRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MGRepositoryImp implements MGRepository {

    private static MGRepositoryImp mgRepositoryImp;
    private final MGService mgService;

    public MGRepositoryImp() {
        mgService = RetrofitService.createService(MGService.class);
    }

    public static MGRepositoryImp newInstance() {
        if (mgRepositoryImp == null) {
            mgRepositoryImp = new MGRepositoryImp();
        }
        return mgRepositoryImp;
    }

    @Override
    public MutableLiveData<String> getMG() {
        MutableLiveData<String> liveData = new MutableLiveData<>();
        mgService.getAllMG().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                liveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
            }
        });

        return liveData;
    }
}
