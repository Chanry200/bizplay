package com.example.bizplay.view.my_card;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.GridLayout;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityMyCardBinding;
import com.example.bizplay.view.main.ViewPager2Adapter;
import com.example.bizplay.view.receipt.ReceiptActivity;
import com.google.android.material.appbar.MaterialToolbar;

import java.util.ArrayList;
import java.util.List;

public class MyCardActivity extends AppCompatActivity {
    private ActivityMyCardBinding binding;

    ViewPagerAdapter viewPagerAdapter;
    ViewPager2 viewPager2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMyCardBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onBack();
        onCardManagement();
        onViewPager2();
        onCardUsageDetail();

    }


    private void onBack(){
        binding.backIcon.setOnClickListener(view -> {
            onBackPressed();
        });
    }

    //Card Management
    private void onCardManagement(){
        binding.cardIcon.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), CardManagementActivity.class);
            startActivity(intent);
        });
    }

    private void onCardUsageDetail(){
        binding.containerCardDetail.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(),CardUsageDetailActivity.class);
            startActivity(intent);

        });
    }

    //ViewPager2
    private void onViewPager2(){
        viewPager2 = findViewById(R.id.card_slide);
        List<OnboardingItem> onboardingItems = new ArrayList<>();
        onboardingItems.add(new OnboardingItem(R.drawable.bg_bc_card));
        onboardingItems.add(new OnboardingItem(R.drawable.bg_bnk_busan_card));
        onboardingItems.add(new OnboardingItem(R.drawable.bg_citi_card));
        viewPagerAdapter = new ViewPagerAdapter (onboardingItems, getApplicationContext(), viewPager2);
        viewPager2.setAdapter(viewPagerAdapter);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

    }
}