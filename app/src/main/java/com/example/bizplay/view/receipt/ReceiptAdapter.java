//package com.example.bizplay.view.receipt;
//
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.bizplay.R;
//
//import java.util.List;
//
//public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ViewHolder> {
//
//    private final List<ReceiptRepo> receiptRepos;
//
//    public ReceiptAdapter(List<ReceiptRepo> receiptRepos) {
//        this.receiptRepos = receiptRepos;
//    }
//
//    @NonNull
//    @Override
//    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(ViewHolder holder, int position) {
//        ReceiptRepo receipt = receiptRepos.get(position);
//        holder.NumberCard.setText(receipt.getCARD_NO());
//        holder.DateReceipt.setText(receipt.getAPV_DT());
//        holder.TitleReceipt.setText(receipt.getMEST_NM());
//        holder.AmountReceipt.setText(receipt.getAPV_AMT());
//        holder.ReceiptProgress.setText(receipt.getTRAN_KIND_NM());
//    }
//
//    @Override
//    public int getItemCount() {
//        return receiptRepos.size();
//    }
//
//    //create View holder
//    public static class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView NumberCard, TitleReceipt, AmountReceipt, DateReceipt, ReceiptProgress;
//
//        public ViewHolder(View view) {
//            super(view);
//            NumberCard = view.findViewById(R.id.txt_NumberCard);
//            TitleReceipt = view.findViewById(R.id.receipt_title);
//            AmountReceipt = view.findViewById(R.id.amount_each_receipt);
//            DateReceipt = view.findViewById(R.id.date_receipt);
//            ReceiptProgress = view.findViewById(R.id.txt_progress);
//        }
//    }
//}
