package com.example.bizplay.view.notification;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityNotificationBinding;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    ActivityNotificationBinding binding;
    ImageView mCloseImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       binding = DataBindingUtil.setContentView(this,R.layout.activity_notification);

       NotificationData();;


        //click to close notification
        mCloseImage = binding.closeImage;
        mCloseImage.setOnClickListener(view -> {
            onBackPressed();
        });

    }

    public void NotificationData(){
        List<NotificationDataModel> notificationDataModelList = new ArrayList<>();

        notificationDataModelList.add(new NotificationDataModel("하이패스 사용내역 3건이 도착했습니다.","19 / 08 / 12"));
        notificationDataModelList.add(new NotificationDataModel("하이패스 사용내역 3건이 도착했습니다.","19 / 08 / 12"));

        NotificationAdapter notificationAdapter = new NotificationAdapter(notificationDataModelList,this);
        binding.setMyAdapter(notificationAdapter);

    }
}