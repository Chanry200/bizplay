package com.example.bizplay.view.intro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.bizplay.R;

import java.util.List;

public class ViewPager2AdapterIntro extends RecyclerView.Adapter<ViewPager2AdapterIntro.ViewHolder> {

    List<OnboardingItemIntro> onboardingItemIntros;
    private Context context;
    private ViewPager2 viewPager2;

    public ViewPager2AdapterIntro(List<OnboardingItemIntro> onboardingItemIntros, Context context, ViewPager2 viewPager2) {
        this.onboardingItemIntros = onboardingItemIntros;
        this.context = context;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_page, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setOnboardingData(onboardingItemIntros.get(position));
    }

    @Override
    public int getItemCount() {
        return onboardingItemIntros.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
       ImageView images;
       TextView titleIntro, aboutIntro,startIntro;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            images = itemView.findViewById(R.id.ivImage);
            titleIntro = itemView.findViewById(R.id.tvTitle);
            aboutIntro = itemView.findViewById(R.id.tvAbout);
//            startIntro = itemView.findViewById(R.id.tv_start);
        }
        void setOnboardingData(OnboardingItemIntro onboardingItemIntro){
            images.setImageResource(onboardingItemIntro.getImages());
            titleIntro.setText(onboardingItemIntro.getTitleIntro());
            aboutIntro.setText(onboardingItemIntro.getAboutIntro());
//            startIntro.setText(onboardingItemIntro.getStartInro());
        }
    }
}
