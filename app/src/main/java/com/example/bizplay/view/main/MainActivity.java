package com.example.bizplay.view.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.bizplay.R;
import com.example.bizplay.view.business_trip.BusinessTripActivity;
import com.example.bizplay.view.expense_report.ExpenseReportActivity;
import com.example.bizplay.view.more.MoreActivity;
import com.example.bizplay.view.my_card.MyCardActivity;
import com.example.bizplay.view.notification.NotificationActivity;
import com.example.bizplay.view.receipt.ReceiptActivity;
import com.google.android.material.appbar.MaterialToolbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ViewPager2Adapter viewPager2Adapter;
    ViewPager2 viewPager2;
    private Handler sliderHandler = new Handler();
    GridLayout gridLayout_receipt, gridLayout_myCard,gridLayout_B_trip,gridLayout_report;
    MaterialToolbar mToolbar;
    private LinearLayout layoutIndicators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        onReceipt();
        onExpenseReport();
        onMyCard();
        onBusinessTrip();
        onNotification();

//set image for viewpager2
        layoutIndicators = findViewById(R.id.indicator_bar);
        viewPager2 = findViewById(R.id.viewpager2);
        List<OnboardingItem> onboardingItems = new ArrayList<>();
        onboardingItems.add(new OnboardingItem(R.drawable.bg_img_1));
        onboardingItems.add(new OnboardingItem(R.drawable.bg_img_2));
        onboardingItems.add(new OnboardingItem(R.drawable.bg_img_3));
        viewPager2Adapter = new ViewPager2Adapter(onboardingItems, getApplicationContext(), viewPager2);
        viewPager2.setAdapter(viewPager2Adapter);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);


        //Indicator
        settingUpIndicators();
        settingCurrentIndicator(0);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                settingCurrentIndicator(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 2000);
            }
        });
    }

    //Use for viewpager2 and indicator
    private Runnable sliderRunnable = new Runnable() {
        @Override
        public void run() {
            ImageView[] indicators = new ImageView[viewPager2Adapter.getItemCount()];
            int slide = indicators.length - 1;
            if (viewPager2.getCurrentItem() >= slide) {
                viewPager2.setCurrentItem(0);
            } else
                viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
            Log.d("Viewpager2", "run: " + viewPager2.getCurrentItem() + slide);
        }
    };

    //Click on notification
    private void onNotification(){
        mToolbar = findViewById(R.id.topAppBarHome);
        OnToolbarItemClick();
    }

    //Receipt
    private void onReceipt(){
        //Click to ReceiptActivity
        gridLayout_receipt = findViewById(R.id.receipt);
        gridLayout_receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ReceiptActivity.class);
                startActivity(intent);
            }
        });
    }

    //Expanse report
    private void onExpenseReport(){
        //Click to Expanse report
        gridLayout_report = findViewById(R.id.report);
        gridLayout_report.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), ExpenseReportActivity.class);
            startActivity(intent);
        });

    }

    //Card
    private void onMyCard(){
        //Click to MyCard
        gridLayout_myCard = findViewById(R.id.mycard);
        gridLayout_myCard.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), MyCardActivity.class);
            startActivity(intent);
        });
    }

    //Business trip
    private void onBusinessTrip(){
        //Click to Business trip
        gridLayout_B_trip = findViewById(R.id.my_business);
        gridLayout_B_trip.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), BusinessTripActivity.class);
            startActivity(intent);

        });
    }

    // indicator
    private void settingUpIndicators() {
        ImageView[] indicators = new ImageView[viewPager2Adapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(10, 10, 10, 10);

        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.indicator_inactive
            ));
            indicators[i].setLayoutParams(layoutParams);
            layoutIndicators.addView(indicators[i]);
        }
    }

    private void settingCurrentIndicator(int index) {
        int childCount = layoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ImageView imageView = (ImageView) layoutIndicators.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.indicator_active));
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.indicator_inactive));
            }
        }
    }
    //Click Toolbar on Homepage
    private void OnToolbarItemClick() {
        mToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.notification_icon:
                    Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                    startActivity(intent);
                    return false;
                case R.id.more_icon:
                    Intent intent_more = new Intent(getApplicationContext(), MoreActivity.class);
                    startActivity(intent_more);
                default:
                    return false;
            }

        });
    }

}