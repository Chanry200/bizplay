package com.example.bizplay.view.main;

public class OnboardingItem {
    public int images;

    public int getImage() {
        return images;
    }

    public OnboardingItem(int images) {
        this.images = images;
    }

    public void setImage(int image) {
        this.images = image;
    }
}
