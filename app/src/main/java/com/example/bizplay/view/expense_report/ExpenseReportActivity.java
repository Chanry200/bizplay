package com.example.bizplay.view.expense_report;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityExpenseReportBinding;
import com.example.bizplay.view.expense_report.floatingButton.ExpenseReportPopupClass;
import com.example.bizplay.view.expense_report.floatingButton.ExpenseReportPopupItem;
import com.example.bizplay.view.receipt.ReceiptActivity;
import com.example.bizplay.view.receipt.floatingButton.PopupClass;
import com.example.bizplay.view.receipt.floatingButton.PopupItem;

public class ExpenseReportActivity extends AppCompatActivity {
    private ActivityExpenseReportBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExpenseReportBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onBack();
        onFloatingButton();

    }

    private void onBack(){
        binding.backIcon.setOnClickListener(view -> {
            onBackPressed();
        });
    }
    private void onFloatingButton() {
        binding.floatingBtn.setOnClickListener(view -> {
            ExpenseReportPopupClass expenseReportPopupClass = new ExpenseReportPopupClass();
            expenseReportPopupClass.onButtonShowPopupWindowClick(view, new ExpenseReportPopupClass.OnMenuClickListener() {
                @Override
                public void onClicked(@NonNull ExpenseReportPopupItem item) {
                    Toast.makeText(ExpenseReportActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                }

            });
        });


    }
}