package com.example.bizplay.view.receipt;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.bizplay.R;
import com.example.bizplay.data.remote.request.BaseRequest;
import com.example.bizplay.data.remote.request.ReceiptReq;
import com.example.bizplay.data.remote.response.RecurrentResponse;
import com.example.bizplay.data.viewmodel.ReceiptViewModel;
import com.example.bizplay.databinding.ActivityReceiptBinding;
import com.example.bizplay.utils.AppConstant;
import com.example.bizplay.view.main.MainActivity;
import com.example.bizplay.view.receipt.floatingButton.PopupClass;
import com.example.bizplay.view.receipt.floatingButton.PopupItem;
import com.example.bizplay.view.receipt.search.SearchReceiptActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReceiptActivity extends AppCompatActivity {
    ReceiptViewModel receiptViewModel;
//    ReceiptAdapter receiptAdapter;
    private ActivityReceiptBinding binding;
    private String getReceiptReq;
    private String[] APV_AMT;
    private String[] MEST_NM;
    private String[] APV_DT;
    private String[] CARD_ORG_NM;
    private String[] CARD_NO;
    private String[] TRAN_KIND_NM;
    private String[] INQ_GRP;

    @SuppressLint({"ResourceType"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityReceiptBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        receiptViewModel = new ViewModelProvider(this).get(ReceiptViewModel.class);
        getReceipt();
        onFloatingButton();
        onBack();
        onSearch();

    }

    private void onFloatingButton() {
        binding.floatingBtn.setOnClickListener(view -> {
            PopupClass popupClass = new PopupClass();
            popupClass.onButtonShowPopupWindowClick(view, new PopupClass.OnMenuClickListener() {
                @Override
                public void onClicked(@NonNull PopupItem item) {
                    Toast.makeText(ReceiptActivity.this, item.getTitle(), Toast.LENGTH_SHORT).show();
                }
            });
        });
    }

    private void onBack() {
        //Back icon to homepage
        binding.backIcon.setOnClickListener(view -> {
            onBackPressed();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        });

    }

    private void onSearch() {
        //Search Receipt
//        binding.fab.setOnClickListener(view -> {
//            Intent intent = new Intent(getApplicationContext(), SearchReceiptActivity.class);
//            startActivity(intent);
//        });
    }


    private void getReceipt() {
        try {

            ReceiptReq receiptReq = new ReceiptReq();
            receiptReq.setINQ_CD("0");
            receiptReq.setINQ_NEXT_APV_DT("");
            receiptReq.setINQ_NEXT_APV_NO("");
            receiptReq.setINQ_NEXT_APV_SEQ("");
            receiptReq.setINQ_NEXT_APV_TM("");
            receiptReq.setINQ_NEXT_CARD_NO("");


            //BaseRequest
            BaseRequest baseRequest = new BaseRequest();
            baseRequest.setCNTS_CRTS_KEY("");
            baseRequest.setTRAN_NO(AppConstant.BPCD_MBL_L_002);
            baseRequest.setREQ_DATA(receiptReq);

            getReceiptReq = "JSONData=" + baseRequest;

            receiptViewModel.getReceipt(getReceiptReq).observe(this, response -> {

                Log.d("ry", "getReceipt:" + response.getRESP_DATA());

                Map<String, ArrayList<RecurrentResponse>> receipts = new LinkedHashMap<>();

                if(response.getRESP_DATA().getINQ_ITRN_SUB() != null){

                    // fake data
                    ArrayList<RecurrentResponse> fakes1 = new ArrayList<>(response.getRESP_DATA().getINQ_ITRN_SUB());
                    ArrayList<RecurrentResponse> fakes2 = new ArrayList<>();
                    for (RecurrentResponse fake : fakes1) {
                        try {
                            RecurrentResponse f = (RecurrentResponse) fake.clone();
                            f.setINQ_GRP("2205");
                            f.setAPV_DT("20220506");
                            fakes2.add(f);
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                    }

                    ArrayList<RecurrentResponse> fakes = new ArrayList<>();
                    fakes.addAll(fakes2);
                    fakes.addAll(fakes1);
                    response.getRESP_DATA().setINQ_ITRN_SUB(fakes);

                    for (int i = 0; i < response.getRESP_DATA().getINQ_ITRN_SUB().size(); i++) {

                        String INQ_GRP1 = response.getRESP_DATA().getINQ_ITRN_SUB().get(i).getINQ_GRP();

                        ArrayList<RecurrentResponse> receipt = new ArrayList<>();

                        for (int j = 0; j < response.getRESP_DATA().getINQ_ITRN_SUB().size(); j++) {

                            String INQ_GRP2 = response.getRESP_DATA().getINQ_ITRN_SUB().get(j).getINQ_GRP();

                            if(INQ_GRP1.equals(INQ_GRP2)){
                                receipt.add(response.getRESP_DATA().getINQ_ITRN_SUB().get(j));
                            }
                        }
                        receipts.put(INQ_GRP1, receipt);
                    }
                }
                ReceiptListAdapter adapter = new ReceiptListAdapter();
                adapter.setList(receipts);
                binding.receiptListview.setAdapter(adapter);

                for (int i = 0; i < receipts.entrySet().size(); i++) {
                    binding.receiptListview.expandGroup(i);
                }

                // disable group expand click
                binding.receiptListview.setOnGroupClickListener((expandableListView, view, i, l) -> true);
            });

        } catch (JSONException exception) {
            exception.printStackTrace();
        }

    }


}