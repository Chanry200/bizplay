package com.example.bizplay.view.receipt.search;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bizplay.R;

public class SearchReceiptActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_receipt);
    }
}