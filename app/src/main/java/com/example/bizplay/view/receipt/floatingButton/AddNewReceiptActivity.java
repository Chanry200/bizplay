package com.example.bizplay.view.receipt.floatingButton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityAddNewReceiptBinding;
import com.example.bizplay.databinding.ActivityCreateExpenseReportBinding;

public class AddNewReceiptActivity extends AppCompatActivity {

    ActivityAddNewReceiptBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_new_receipt);
        binding = ActivityAddNewReceiptBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

    }
}