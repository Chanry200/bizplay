package com.example.bizplay.view.more;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityMoreBinding;

public class MoreActivity extends AppCompatActivity {

    private ActivityMoreBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMoreBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onBack();
    }

    private void onBack(){
        binding.backIcon.setOnClickListener(view -> {
            onBackPressed();
        });
    }
}