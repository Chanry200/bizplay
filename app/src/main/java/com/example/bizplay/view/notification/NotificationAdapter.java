package com.example.bizplay.view.notification;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bizplay.BR;
import com.example.bizplay.R;
import com.example.bizplay.databinding.NotifiationViewItemBinding;
import com.example.bizplay.listener.CustomClickListener;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> implements CustomClickListener {

    private List<NotificationDataModel> notificationDataModelList;
    private Context context;

    public NotificationAdapter(List<NotificationDataModel> notificationDataModelList, Context ctx) {
        this.notificationDataModelList = notificationDataModelList;
        this.context = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        NotifiationViewItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.notifiation_view_item,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationDataModel dataModel = notificationDataModelList.get(position);
        holder.bind(dataModel);
        holder.notifiationViewItemBinding.setItemClickListener(this);

    }

    @Override
    public int getItemCount() {
        return notificationDataModelList.size();
    }

    @Override
    public void linearClicked(NotificationDataModel f) {
        Toast.makeText(context, "You clicked " + f.NotificationTitle,
                Toast.LENGTH_LONG).show();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public NotifiationViewItemBinding notifiationViewItemBinding;

        public ViewHolder(NotifiationViewItemBinding notifiationViewItemBinding) {
            super(notifiationViewItemBinding.getRoot());
            this.notifiationViewItemBinding = notifiationViewItemBinding;
        }

        public void bind(Object obj){
            notifiationViewItemBinding.setVariable(BR.model,obj);
            notifiationViewItemBinding.executePendingBindings();
        }
    }
}
