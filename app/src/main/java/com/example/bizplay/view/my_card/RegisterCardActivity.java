package com.example.bizplay.view.my_card;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityRegisterCardBinding;

public class RegisterCardActivity extends AppCompatActivity {

    ActivityRegisterCardBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       binding = ActivityRegisterCardBinding.inflate(getLayoutInflater());
       setContentView(binding.getRoot());
       onBack();
       onCancel();
    }

    private void onBack(){
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void onCancel(){
        binding.closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}