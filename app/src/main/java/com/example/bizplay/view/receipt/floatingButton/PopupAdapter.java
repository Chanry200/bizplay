package com.example.bizplay.view.receipt.floatingButton;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;

import com.example.bizplay.databinding.ItemMenuBinding;

import java.util.ArrayList;
import java.util.List;

public class PopupAdapter extends BaseAdapter {

    private final List<PopupItem> popupItems;

    public PopupAdapter(){
        popupItems = new ArrayList<>();
    }

    public PopupAdapter(@NonNull List<PopupItem> popupItems) {
        this.popupItems = popupItems;
    }

    @Override
    public int getCount() {
        return popupItems.size();
    }

    @Override
    public Object getItem(int i) {
        return popupItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        PopupViewHolder holder;
        if(view == null){
            ItemMenuBinding binding = ItemMenuBinding.inflate(LayoutInflater.from(viewGroup.getContext()));
            binding.setMenu(popupItems.get(i));

            holder = new PopupViewHolder(binding);
            holder.view = binding.getRoot();
            holder.view.setTag(holder);
        }else{
            holder = (PopupViewHolder) view.getTag();
        }

        return holder.view;
    }

    static class PopupViewHolder {
        private View view;
        public PopupViewHolder(@NonNull ItemMenuBinding itemView) {
            view = itemView.getRoot();
        }
    }
}
