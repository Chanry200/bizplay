package com.example.bizplay.view.notification;

public class NotificationDataModel {
    public String NotificationTitle, NotificationDate;

    public NotificationDataModel(String notificationTitle, String notificationDate) {
        NotificationTitle = notificationTitle;
        NotificationDate = notificationDate;
    }
}
