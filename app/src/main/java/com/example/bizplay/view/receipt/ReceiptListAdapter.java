package com.example.bizplay.view.receipt;

import android.view.LayoutInflater;;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import androidx.annotation.NonNull;

import com.example.bizplay.data.remote.response.RecurrentResponse;
import com.example.bizplay.databinding.RecyclerViewHeaderItemBinding;
import com.example.bizplay.databinding.RecyclerViewItemBinding;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReceiptListAdapter extends BaseExpandableListAdapter {

    public static final String TAG = "Test";
    private final ArrayList<RecurrentResponse> mChildItem;
    private final ArrayList<ReceiptHeader> mGroupItem;
    private final Map<String, ArrayList<RecurrentResponse>> mList;
    private boolean isCheckStatus;

    public ReceiptListAdapter() {
        mList = new LinkedHashMap<>();
        mGroupItem = new ArrayList<>();
        mChildItem = new ArrayList<>();
    }

    public void setList(@NonNull Map<String, ArrayList<RecurrentResponse>> receipts) {
        mList.putAll(receipts);
        for (Map.Entry<String, ArrayList<RecurrentResponse>> entry : receipts.entrySet()) {

            // header
            ReceiptHeader header = new ReceiptHeader(entry.getKey());
            mGroupItem.add(header);
            mChildItem.addAll(entry.getValue());
        }
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return mList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        ArrayList<RecurrentResponse> r = mList.get(mGroupItem.get(i).getName());
        if (r == null) r = new ArrayList<>();
        return r.size();
    }

    @Override
    public Object getGroup(int i) {
        return mGroupItem.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        ArrayList<RecurrentResponse> r = mList.get(mGroupItem.get(i).getName());
        if (r == null) r = new ArrayList<>();
        return r.get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    //Header of receipt
    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        ReceiptHeader receiptHeader = mGroupItem.get(i);
        ArrayList<RecurrentResponse> response = mList.get(receiptHeader.getName());

        RecyclerViewHeaderItemBinding binding = RecyclerViewHeaderItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);

        if (response != null && !response.isEmpty()){

            binding.setHeader(receiptHeader.getName());

            binding.checkHeader.setChecked(receiptHeader.isChecked());

            //TODO click header Item for enable all child checkbox
            binding.checkHeader.setOnCheckedChangeListener((compoundButton, isChecked) -> {

                // remember header check
                receiptHeader.setChecked(isChecked);

                // check child
                for (RecurrentResponse rec : response) {
                    rec.setChecked(isChecked);
                }

                notifyDataSetChanged();
            });
        }

        return binding.getRoot();
    }

    //All Receipt
    @Override
    public View getChildView(int parentPos, int childPos, boolean b, View view, ViewGroup viewGroup) {
        RecyclerViewItemBinding binding = RecyclerViewItemBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        binding.divider.setVisibility(childPos == 0 ? View.GONE : View.VISIBLE);

        ReceiptHeader receiptHeader = mGroupItem.get(parentPos);

        ArrayList<RecurrentResponse> children = mList.get(mGroupItem.get(parentPos).getName());
        if (children == null) children = new ArrayList<>();
        binding.setReceipt(children.get(childPos));
        RecurrentResponse child = children.get(childPos);

        // set on long press
        binding.getRoot().setOnLongClickListener(view1 -> {
            binding.chk.setVisibility(View.VISIBLE);
            setCheckStatus(true);
            return false;
        });

        // child check
        binding.getRoot().setOnClickListener(view1 -> binding.chk.performClick());
        if (isCheckStatus) {
            binding.chk.setVisibility(View.VISIBLE);
        }
        binding.chk.setChecked(child.isChecked());

        // check listener
        ArrayList<RecurrentResponse> finalChildren = children;
        binding.chk.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            child.setChecked(isChecked);
            receiptHeader.setChecked(isChildrenAllChecked(finalChildren));
            notifyDataSetChanged();
        });
        return binding.getRoot();
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    public void setCheckStatus(boolean isChecked) {
        isCheckStatus = isChecked;
        notifyDataSetChanged();
    }

    private boolean isChildrenAllChecked(@NonNull ArrayList<RecurrentResponse> children){
        boolean isAllChecked = true;
        for (RecurrentResponse child : children) {
            if(!child.isChecked()){
                isAllChecked = false;
                break;
            }
        }
        return isAllChecked;
    }
}
