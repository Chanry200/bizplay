package com.example.bizplay.view.intro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.bizplay.R;
import com.example.bizplay.view.main.MainActivity;
import com.example.bizplay.view.signin.SignInActivity;

import java.util.ArrayList;
import java.util.List;

public class IntroScreenActivity extends AppCompatActivity {

    List<OnboardingItemIntro> onboardingItemIntros;
    ViewPager2AdapterIntro viewPager2AdapterIntro;
    ViewPager2 viewPager2;
    ImageView imageView;
    TextView textView;
    private final Handler sliderHandler = new Handler();
    private LinearLayout layoutIndicators, linearLayoutStart;


    //Use for viewpager2 and indicator
    private final Runnable sliderRunnable = new Runnable() {
        @Override
        public void run() {
            ImageView[] indicators = new ImageView[viewPager2AdapterIntro.getItemCount()];
            int slide = indicators.length - 1;
            if (viewPager2.getCurrentItem() >= slide) {
                viewPager2.setCurrentItem(0);
            } else
                viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
            Log.d("Viewpager2", "run: " + viewPager2.getCurrentItem() + slide);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_screen);

        //Back button
        imageView =findViewById(R.id.back_icon);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //layout start
        linearLayoutStart = findViewById(R.id.linear_start);
        linearLayoutStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                startActivity(intent);
            }
        });

        //set image for viewpager2
        layoutIndicators = findViewById(R.id.indicator_intro);
        viewPager2 = findViewById(R.id.intro_viewPager2);

        List<OnboardingItemIntro> onboardingItemIntros = new ArrayList<>();

        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_01,"더욱 편리해진\n" +
                "비즈플레이를 소개합니다.","카테고리별로 업무를 선택하여\n" +
                "더욱 쉽고 빠른 경비처리가 가능합니다",""));
        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_02,"영수증 통합 조회","법인카드, 개인카드, 기타간이(현금), 티머니, \n" +
                "제로페이 영수증을 통합 조회할 수 있어 한눈에 \n" +
                "내역을 볼 수 있습니다\n",""));
        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_03,"결의서 작성 및 조회","결의서 메뉴에서 결의서 작성과\n" +
                "MY 기안문서 조회가 가능합니다",""));
        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_04,"내 카드 한눈에 조회","사용자로 지정된 MY카드를 한번에 보고,\n" +
                "카드별로 사용금액 및 사용내역을 조회할 수 있습니다",""));
        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_05,"영수증에 구매 상세정보 제공","철도, 숙박, 온라인몰, 퀵서비스 등\n" +
                "구매 상세정보를 영수증에 제공하여 경비 처리 업무를 \n" +
                "간소화합니다.",""));
        onboardingItemIntros.add(new OnboardingItemIntro(R.drawable.img_guide_06,"비즈플레이","법인카드, 개인카드, 기타 영수증이 통합되어\n" +
                "하나의 앱으로 빠른 경비처리를 할 수 있는 \n" +
                "비즈플레이 앱을 지금 경험해보세요!","START"));


        viewPager2AdapterIntro = new ViewPager2AdapterIntro(onboardingItemIntros, getApplicationContext(), viewPager2);
        viewPager2.setAdapter(viewPager2AdapterIntro);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);


        //Indicator
        settingUpIndicators();
        settingCurrentIndicator(0);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                settingCurrentIndicator(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 2000);
            }
        });
    }

    // indicator
    private void settingUpIndicators() {
        ImageView[] indicators = new ImageView[viewPager2AdapterIntro.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(10, 10, 10, 10);

        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.indicator_inactive_intro
            ));
            indicators[i].setLayoutParams(layoutParams);
            layoutIndicators.addView(indicators[i]);
        }
    }

    private void settingCurrentIndicator(int index) {
        int childCount = layoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ImageView imageView = (ImageView) layoutIndicators.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.indicator_active_intro));
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.indicator_inactive_intro));
            }
        }
    }
}