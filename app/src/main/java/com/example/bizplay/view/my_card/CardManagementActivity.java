package com.example.bizplay.view.my_card;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityCardManagementBinding;

public class CardManagementActivity extends AppCompatActivity {

    ActivityCardManagementBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_card_management);
        binding = ActivityCardManagementBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onBack();
        onAddCard();
    }

    private void onBack(){
        binding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void onAddCard(){
        binding.addCardIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),RegisterCardActivity.class);
                startActivity(intent);

            }
        });
    }
}