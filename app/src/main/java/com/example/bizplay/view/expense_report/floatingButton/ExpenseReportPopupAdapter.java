package com.example.bizplay.view.expense_report.floatingButton;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.annotation.NonNull;

import com.example.bizplay.databinding.ItemExpenseReportMenuBinding;
import com.example.bizplay.databinding.ItemMenuBinding;

import java.util.ArrayList;
import java.util.List;

public class ExpenseReportPopupAdapter extends BaseAdapter {

    private final List<ExpenseReportPopupItem> popupItems;

    public ExpenseReportPopupAdapter(){
        popupItems = new ArrayList<>();
    }

    public ExpenseReportPopupAdapter(@NonNull List<ExpenseReportPopupItem> popupItems) {
        this.popupItems = popupItems;
    }

    @Override
    public int getCount() {
        return popupItems.size();
    }

    @Override
    public Object getItem(int i) {
        return popupItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        PopupViewHolder holder;
        if(view == null){
            ItemExpenseReportMenuBinding binding = ItemExpenseReportMenuBinding.inflate(LayoutInflater.from(viewGroup.getContext()));
            binding.setExpense(popupItems.get(i));

            holder = new PopupViewHolder(binding);
            holder.view = binding.getRoot();
            holder.view.setTag(holder);
        }else{
            holder = (PopupViewHolder) view.getTag();
        }

        return holder.view;
    }

    static class PopupViewHolder {
        private View view;
        public PopupViewHolder(@NonNull ItemExpenseReportMenuBinding itemView) {
            view = itemView.getRoot();
        }
    }
}
