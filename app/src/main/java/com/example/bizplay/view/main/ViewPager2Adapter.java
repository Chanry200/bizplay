package com.example.bizplay.view.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.bizplay.R;

import java.util.List;

public class ViewPager2Adapter extends RecyclerView.Adapter<ViewPager2Adapter.ViewHolder> {

    List<OnboardingItem> onboardingItems;
    private Context context;
    private ViewPager2 viewPager2;

    public ViewPager2Adapter(List<OnboardingItem> onboardingItems, Context context, ViewPager2 viewPager2) {
        this.onboardingItems = onboardingItems;
        this.context = context;
        this.viewPager2 = viewPager2;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.slider_list, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setOnboardingData(onboardingItems.get(position));
    }

    @Override
    public int getItemCount() {
        return onboardingItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
       ImageView images;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            images = itemView.findViewById(R.id.slide_image);
        }
        void setOnboardingData(OnboardingItem onboardingItem){
            images.setImageResource(onboardingItem.getImage());
        }
    }
}
