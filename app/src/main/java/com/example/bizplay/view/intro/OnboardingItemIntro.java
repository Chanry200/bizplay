package com.example.bizplay.view.intro;

public class OnboardingItemIntro {
    private int images;
    private String titleIntro, aboutIntro, startInro ;

    public OnboardingItemIntro(int images, String titleIntro, String aboutIntro,String startInro) {
        this.images = images;
        this.titleIntro = titleIntro;
        this.aboutIntro = aboutIntro;
        this.startInro =startInro;
    }

    public String getStartInro() {
        return startInro;
    }

    public void setStartInro(String startInro) {
        this.startInro = startInro;
    }

    public int getImages() {
        return images;
    }

    public void setImages(int images) {
        this.images = images;
    }

    public String getTitleIntro() {
        return titleIntro;
    }

    public void setTitleIntro(String titleIntro) {
        this.titleIntro = titleIntro;
    }

    public String getAboutIntro() {
        return aboutIntro;
    }

    public void setAboutIntro(String aboutIntro) {
        this.aboutIntro = aboutIntro;
    }
}
