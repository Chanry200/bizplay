package com.example.bizplay.view.signin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.bizplay.R;
import com.example.bizplay.data.remote.request.BaseRequest;
import com.example.bizplay.data.remote.request.LoginReq;
import com.example.bizplay.data.remote.request.LoginRequest;
import com.example.bizplay.data.viewmodel.LoginViewModel;
import com.example.bizplay.databinding.ActivitySigninBinding;
import com.example.bizplay.utils.AppConstant;
import com.example.bizplay.utils.EncryptUtil;
import com.example.bizplay.view.WebView.WebViewActivity;
import com.example.bizplay.view.intro.IntroScreenActivity;
import com.example.bizplay.view.main.MainActivity;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SignInActivity extends AppCompatActivity {

    String url;
    LoginViewModel loginViewModel;
    LoginRequest loginRequest;
    String loginEncode;
    private ActivitySigninBinding binding;
    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySigninBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        onClickIntroScreen();
        onJoinMember();
        onForgotPassword();
        onForgotUserId();
        onClickLoginWithCompanyCode();
        onLogin();

    }

    public void onLogin() {
        binding.buttonLogin.setOnClickListener(view -> {
            String usernameId = "simdemo01t";
            //String usernameId = binding.userNameId.getText().toString();
            // String password = binding.password.getText().toString();
            String password = "335ffabb948bb391d72d24ad61c7617c7220ccf6b14dbb399279706bff556df601fbfbcf331873d4733dac4d0b48600a3299d9ea8f13e3c842085183c31f0b5e3c78568e4ee735a88d42ecdd6b7e94177104bf52c4b9fd1f1b30e761e4c82d150e32300b9632069737a3b4210033ce089cfba5bf62c77af689e615504c91212188afb278b2a7863aa9cdc19d7e9e2532";

            //Condition when login
//            TODO login Request
            try {
                LoginReq loginReq = new LoginReq();
                loginReq.setUSE_INTT_ID("");
                loginReq.setUSER_ID(usernameId);
                loginReq.setUSER_PW(password);
                loginReq.setENC_GB("1");
                loginReq.setMOBL_CD("0");
                loginReq.setSESSION_ID("");
                loginReq.setTOKEN("");
                loginReq.setNATION_CD("");

                //BaseRequest
                BaseRequest baseRequest = new BaseRequest();
                baseRequest.setCNTS_CRTS_KEY("");
                baseRequest.setTRAN_NO(AppConstant.BPCD_MBL_P001);
                baseRequest.setREQ_DATA(loginReq);

                //Check condition
                if (usernameId.isEmpty()) {
                    showDialog();
                } else {
                    binding.loginLoading.setVisibility(View.VISIBLE);
                    loginEncode = "JSONData=" + URLEncoder.encode(baseRequest.toString(), "UTF-8");
                    loginRequest = new LoginRequest(loginEncode);
                    loginViewModel.login(loginEncode).observe(this, loginResponsePostResponse -> {
                        SharedPreferences.Editor editorLogin = getApplicationContext().getSharedPreferences(AppConstant.MY_LOGIN, MODE_PRIVATE).edit();
                        editorLogin.putString(AppConstant.USER_INTT_ID, loginResponsePostResponse.getRESP_DATA().getUSE_INTT_ID());
                        editorLogin.putString(AppConstant.BODY_LOGIN, loginEncode);
                        editorLogin.apply();

                        if (loginResponsePostResponse.getRSLT_CD().equals("0000")) {

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            showDialog();
                        }
                        binding.loginLoading.setVisibility(View.GONE);
                    });
                }
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        });
    }

    private void onClickIntroScreen() {
        binding.backLogin.setOnClickListener(view -> {
            Intent intent = new Intent(SignInActivity.this, IntroScreenActivity.class);
            startActivity(intent);
        });
    }

    private void onClickLoginWithCompanyCode() {
        binding.loginWithWorkCode.setOnClickListener(view -> {
            Intent intent = new Intent(SignInActivity.this, LoginWithCompanyCodeActivity.class);
            startActivity(intent);
        });
    }

    private void onJoinMember() {
        binding.joinMembership.setOnClickListener(view -> {
            SharedPreferences sharedPreferences = getSharedPreferences("MG", MODE_PRIVATE);
            url = sharedPreferences.getString("c_member_url", "");
            Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
            intent.putExtra("url", url);
            startActivity(intent);
        });
    }

    private void onForgotPassword() {
        binding.forgotPassword.setOnClickListener(view -> {
            SharedPreferences sharedPreferences = getSharedPreferences("MG", MODE_PRIVATE);
            url = sharedPreferences.getString("c_forget_pw_url", "");
            Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
            intent.putExtra("url", url);
            startActivity(intent);
        });
    }

    private void onForgotUserId() {
        binding.forgotUserId.setOnClickListener(view -> {
            SharedPreferences sharedPreferences = getSharedPreferences("MG", MODE_PRIVATE);
            url = sharedPreferences.getString("c_forget_id_url", "");
            Intent intent = new Intent(getApplicationContext(), WebViewActivity.class);
            intent.putExtra("url", url);
            startActivity(intent);
        });
    }

    private void showDialog() {
        alert = new AlertDialog.Builder(this);
        alert.setTitle("");

        final View customLayout = getLayoutInflater().inflate(R.layout.dailog_alert_login, null);
        alert.setView(customLayout);
        Button button = customLayout.findViewById(R.id.btn_confirm);
        button.setOnClickListener(view -> {
            onBackPressed();
            Intent intent = new Intent(getApplicationContext(),SignInActivity.class);
            startActivity(intent);
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }

}
