package com.example.bizplay.view.splash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.bizplay.R;
import com.example.bizplay.data.viewmodel.SplashViewModel;
import com.example.bizplay.utils.AppConstant;
import com.example.bizplay.view.signin.SignInActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;

public class SplashScreenActivity extends AppCompatActivity {
    private SplashViewModel splashViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        splashViewModel = new ViewModelProvider(this).get(SplashViewModel.class);
        requestMG();
        splashscreen();

    }

    //Splash screen
    private void splashscreen() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, SignInActivity.class));
                finish();
            }
        }, 2000);
    }

    //RequestMG
    private void requestMG() {
        splashViewModel.getMG().observe(this, response -> {
            Log.d("getMG", "getMG: " + response);
            JSONObject jsonObject;

            try {
                jsonObject = new JSONObject(URLDecoder.decode(response.replaceAll("%(?![0-9a-fA-F]{2})", "%25"), "UTF-8"));
                jsonObject = jsonObject.getJSONObject("RESP_DATA");
                JSONArray jsonArray = jsonObject.getJSONArray("_tran_res_data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    Iterator iterator = jsonObject1.keys();

                    boolean array = false;
                    while ((iterator.hasNext())) {
                        String key = (String) (iterator.next());

                        if (!array) {
                            String value = jsonObject1.getString(key);
                            Log.e("key", "onResponse: key : value " + key + " and " + value);

                            SharedPreferences.Editor editor = getApplicationContext()
                                    .getSharedPreferences(AppConstant.MG, Context.MODE_PRIVATE)
                                    .edit();
                            editor.putString(key, value);
                            editor.apply();

                            array = true;
                        } else {
                            String value = jsonObject1.getString(key);
                            Log.e("key", "onResponse: key:value " + key + " and " + value);
                            SharedPreferences.Editor editor = getApplicationContext()
                                    .getSharedPreferences(AppConstant.MG, Context.MODE_PRIVATE)
                                    .edit();
                            editor.putString(key, value);
                            editor.apply();
                            array = false;
                        }


                    }
                }
                Log.d("re_tran_res_data", ": re_tran_res_data" + jsonArray.toString(0));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        });

    }

}


