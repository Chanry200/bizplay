package com.example.bizplay.view.expense_report.floatingButton;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityAddNewReceiptBinding;
import com.example.bizplay.databinding.ActivityCreateExpenseReportBinding;

public class AddNewExpenseReportActivity extends AppCompatActivity {

  ActivityCreateExpenseReportBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_expense_report);
        binding = ActivityCreateExpenseReportBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

    }
}