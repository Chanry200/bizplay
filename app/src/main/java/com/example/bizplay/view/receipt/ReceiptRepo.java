package com.example.bizplay.view.receipt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReceiptRepo {

    private String APV_AMT;
    private String APV_DT;
    private String CARD_ORG_NM;
    private String MEST_NM;
    private String CARD_NO;
    private String TRAN_KIND_NM;

    public ReceiptRepo(String APV_AMT, String APV_DT, String CARD_ORG_NM, String MEST_NM, String CARD_NO, String TRAN_KIND_NM) {
        this.APV_AMT = APV_AMT;
        this.APV_DT = APV_DT;
        this.CARD_ORG_NM = CARD_ORG_NM;
        this.MEST_NM = MEST_NM;
        this.CARD_NO = CARD_NO;
        this.TRAN_KIND_NM = TRAN_KIND_NM;
    }

    public String getAPV_AMT() {
        return APV_AMT;
    }

    public void setAPV_AMT(String APV_AMT) {
        this.APV_AMT = APV_AMT;
    }

    public String getAPV_DT() {
        return APV_DT;
    }

    public void setAPV_DT(String APV_DT) {
        this.APV_DT = APV_DT;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getMEST_NM() {
        return MEST_NM;
    }

    public void setMEST_NM(String MEST_NM) {
        this.MEST_NM = MEST_NM;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getTRAN_KIND_NM() {
        return TRAN_KIND_NM;
    }

    public void setTRAN_KIND_NM(String TRAN_KIND_NM) {
        this.TRAN_KIND_NM = TRAN_KIND_NM;
    }
}
