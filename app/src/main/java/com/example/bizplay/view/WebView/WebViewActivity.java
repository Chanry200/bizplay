package com.example.bizplay.view.WebView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bizplay.databinding.ActivityWebViewBinding;
import com.example.bizplay.utils.BrowserBridge;

public class WebViewActivity extends AppCompatActivity {
    private ActivityWebViewBinding binding;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWebViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Intent intent = getIntent();
        String url = intent.getExtras().getString("url");
        onClickBack();

        binding.webView.loadUrl(url);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.getSettings().setDomStorageEnabled(true);
        binding.webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        binding.webView.setVerticalScrollBarEnabled(false);
        binding.webView.setHorizontalScrollBarEnabled(false);
        binding.webView.setWebViewClient(new WebViewClient());
        binding.webView.addJavascriptInterface(new BrowserBridge(WebViewActivity.this), "BrowserBridge");
    }

    private void onClickBack() {
        binding.back.setOnClickListener(view -> {
            onClickBack();
            finish();
        });

    }
}