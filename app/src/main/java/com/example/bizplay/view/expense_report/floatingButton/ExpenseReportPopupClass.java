package com.example.bizplay.view.expense_report.floatingButton;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityAddNewReceiptBinding;
import com.example.bizplay.databinding.ActivityCreateExpenseReportBinding;

import java.util.ArrayList;
import java.util.List;

public class ExpenseReportPopupClass {
    private OnMenuClickListener listener;

    //PopupWindow display method
    public void onButtonShowPopupWindowClick(@NonNull View view, OnMenuClickListener listener) {
        this.listener = listener;

        //Create a View object yourself through inflater
        Context context = view.getContext();
        ActivityCreateExpenseReportBinding binding = ActivityCreateExpenseReportBinding.inflate(LayoutInflater.from(context));

        Dialog dialog = new Dialog(context);

        List<ExpenseReportPopupItem> popupItems = new ArrayList<>();
        popupItems.add(new ExpenseReportPopupItem(ContextCompat.getDrawable(context, R.drawable.ic_approval_copy), "지출결의서\n" +
                "(법인카드)"));
        popupItems.add(new ExpenseReportPopupItem(ContextCompat.getDrawable(context, R.drawable.ic_receipt_report), "지출결의서\n" +
                "(기타-간이)"));
        popupItems.add(new ExpenseReportPopupItem(ContextCompat.getDrawable(context, R.drawable.ic_briefcase), "국내 출장정산서"));
        popupItems.add(new ExpenseReportPopupItem(ContextCompat.getDrawable(context, R.drawable.ic_briefcase), "해외 출장정산서"));
       ExpenseReportPopupAdapter adapter = new ExpenseReportPopupAdapter(popupItems);

        binding.containerGridPopup.setAdapter(adapter);
        binding.containerGridPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                listener.onClicked(popupItems.get(i));
            }
        });

        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    //Callback
    public interface OnMenuClickListener {
        void onClicked(@NonNull ExpenseReportPopupItem item);
    }
}
