package com.example.bizplay.view.business_trip;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityBusinessTripBinding;
import com.example.bizplay.databinding.ActivityExpenseReportBinding;
import com.example.bizplay.view.main.MainActivity;

public class BusinessTripActivity extends AppCompatActivity {
    private ActivityBusinessTripBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBusinessTripBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        onBack();

    }

    private void onBack(){
        binding.backIcon.setOnClickListener(view -> {
            onBackPressed();
        });
    }
}