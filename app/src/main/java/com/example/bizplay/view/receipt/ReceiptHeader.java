package com.example.bizplay.view.receipt;

public class ReceiptHeader {
    private static int id = 0;
    private String name;
    private boolean isChecked;

    public ReceiptHeader(String name, boolean isChecked) {
        id++;
        this.name = name;
        this.isChecked = isChecked;
    }

    public ReceiptHeader(String name) {
        id++;
        this.name = name;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        ReceiptHeader.id = id;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
