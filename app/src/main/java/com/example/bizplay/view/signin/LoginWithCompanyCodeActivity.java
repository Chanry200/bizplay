package com.example.bizplay.view.signin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityLoginWithCompanyCodeBinding;

public class LoginWithCompanyCodeActivity extends AppCompatActivity {

    private ActivityLoginWithCompanyCodeBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginWithCompanyCodeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        backIcon();
    }
    public void backIcon(){
        binding.backIcon.setOnClickListener(view -> onBackPressed());
    }
}