package com.example.bizplay.view.receipt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.bizplay.R;

public class CreateReceiptActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_receipt);
    }
}