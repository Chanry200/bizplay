package com.example.bizplay.view.expense_report.floatingButton;

import android.graphics.drawable.Drawable;

public class ExpenseReportPopupItem {
    private Drawable icon;
    private String title;

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ExpenseReportPopupItem(Drawable icon, String title) {
        this.icon = icon;
        this.title = title;
    }
}
