package com.example.bizplay.view.receipt.floatingButton;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.bizplay.R;
import com.example.bizplay.databinding.ActivityAddNewReceiptBinding;

import java.util.ArrayList;
import java.util.List;

public class PopupClass {
    private OnMenuClickListener listener;

    //PopupWindow display method
    public void onButtonShowPopupWindowClick(@NonNull View view, OnMenuClickListener listener) {
        this.listener = listener;

        //Create a View object yourself through inflater
        Context context = view.getContext();
        ActivityAddNewReceiptBinding binding = ActivityAddNewReceiptBinding.inflate(LayoutInflater.from(context));

        Dialog dialog = new Dialog(context);

        List<PopupItem> popupItems = new ArrayList<>();
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_receipt_black), "원화"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_currency), "외화"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_ocr), "사진 스캔"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_photo), "사진보관"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_biz_trip), "출장비"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_car), "교통비"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_gas), "유류비"));
        popupItems.add(new PopupItem(ContextCompat.getDrawable(context, R.drawable.ic_hipass), "하이패스"));
        PopupAdapter adapter = new PopupAdapter(popupItems);

        binding.containerGridPopup.setAdapter(adapter);
        binding.containerGridPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                listener.onClicked(popupItems.get(i));
            }
        });

        dialog.setContentView(binding.getRoot());
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    //Callback
    public interface OnMenuClickListener {
        void onClicked(@NonNull PopupItem item);
    }
}
