package com.example.bizplay.data.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.repositoryImp.MGRepositoryImp;

public class SplashViewModel extends AndroidViewModel {

    private final MGRepositoryImp mgRepositoryImp;
    MutableLiveData<String> mutableLiveData;

    public SplashViewModel(@NonNull Application application) {
        super(application);
        mgRepositoryImp = MGRepositoryImp.newInstance();
    }

    public LiveData<String> getMG()
    {
        mutableLiveData = mgRepositoryImp.getMG();
        return mutableLiveData;
    }
}
