package com.example.bizplay.data.remote.request;

import org.json.JSONException;
import org.json.JSONObject;

public class ReceiptReq extends JSONObject {

    public void setINQ_CD(String INQ_CD) throws JSONException {
        put("INQ_CD",INQ_CD);
    }

    public void setINQ_NEXT_CARD_NO(String INQ_NEXT_CARD_NO) throws JSONException {
        put("INQ_NEXT_CARD_NO",INQ_NEXT_CARD_NO);
    }

    public void setINQ_NEXT_APV_DT(String INQ_NEXT_APV_DT) throws JSONException {
        put("INQ_NEXT_APV_DT",INQ_NEXT_APV_DT);
    }

    public void setINQ_NEXT_APV_SEQ(String INQ_NEXT_APV_SEQ) throws JSONException {
        put("INQ_NEXT_APV_SEQ",INQ_NEXT_APV_SEQ);
    }

    public void setINQ_NEXT_APV_TM(String INQ_NEXT_APV_TM) throws JSONException {
        put("INQ_NEXT_APV_TM",INQ_NEXT_APV_TM);
    }

    public void setINQ_NEXT_APV_NO(String INQ_NEXT_APV_NO) throws JSONException {
        put("INQ_NEXT_APV_NO",INQ_NEXT_APV_NO);
    }
}
