package com.example.bizplay.data.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.data.remote.response.PostResponse;
import com.example.bizplay.data.remote.response.ReceiptResponse;

public interface ReceiptRepository {
    MutableLiveData<PostResponse<ReceiptResponse>> getReceipt (String receipt);
}
