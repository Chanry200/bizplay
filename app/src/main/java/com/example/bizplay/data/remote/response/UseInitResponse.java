package com.example.bizplay.data.remote.response;

import com.google.gson.annotations.SerializedName;

public class UseInitResponse {

    @SerializedName("JOIN_USE_INTT_ID")
    private String JOIN_USE_INTT_ID;

    @SerializedName("JOIN_BSNN_NM")
    private String JOIN_BSNN_NM;

    public String getJOIN_USE_INTT_ID() {
        return JOIN_USE_INTT_ID;
    }

    public void setJOIN_USE_INTT_ID(String JOIN_USE_INTT_ID) {
        this.JOIN_USE_INTT_ID = JOIN_USE_INTT_ID;
    }

    public String getJOIN_BSNN_NM() {
        return JOIN_BSNN_NM;
    }

    public void setJOIN_BSNN_NM(String JOIN_BSNN_NM) {
        this.JOIN_BSNN_NM = JOIN_BSNN_NM;
    }
}
