package com.example.bizplay.data.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.bizplay.data.remote.response.LoginResponse;
import com.example.bizplay.data.remote.response.PostResponse;
import com.example.bizplay.repositoryImp.LoginImp;

public class LoginViewModel extends AndroidViewModel {
    LoginImp loginImp;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        loginImp = LoginImp.newInstance();
    }

    public LiveData<PostResponse<LoginResponse>> login (String login){
        return loginImp.login(login);
    }

}
