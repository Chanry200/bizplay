package com.example.bizplay.data.remote.request;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseRequest extends JSONObject {
    public void setCNTS_CRTS_KEY(String CNTS_CRTS_KEY) throws JSONException {
        put("CNTS_CRTS_KEY",CNTS_CRTS_KEY);
    }

    public void setTRAN_NO(String TRAN_NO) throws JSONException {
        put("TRAN_NO",TRAN_NO);
    }

    public void setREQ_DATA(JSONObject REQ_DATA) throws JSONException {
        put("REQ_DATA",REQ_DATA);
    }
}
