package com.example.bizplay.data.remote.request;

public class LoginRequest {
    private String formEncode;

    public String getFormEncode() {
        return formEncode;
    }

    public void setFormEncode(String formEncode) {
        this.formEncode = formEncode;
    }

    public LoginRequest(String formEncode) {
        this.formEncode = formEncode;
    }
}
