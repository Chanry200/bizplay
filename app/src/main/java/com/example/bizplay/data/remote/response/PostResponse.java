package com.example.bizplay.data.remote.response;

import com.google.gson.annotations.SerializedName;

public class PostResponse<D> {
    @SerializedName("RSLT_CD")
    private String RSLT_CD;

    @SerializedName("RSLT_MSG")
    private String RSLT_MSG;

    @SerializedName("RESP_DATA")
    private D RESP_DATA;

    public String getRSLT_CD() {
        return RSLT_CD;
    }

    public void setRSLT_CD(String RSLT_CD) {
        this.RSLT_CD = RSLT_CD;
    }

    public String getRSLT_MSG() {
        return RSLT_MSG;
    }

    public void setRSLT_MSG(String RSLT_MSG) {
        this.RSLT_MSG = RSLT_MSG;
    }


    public D getRESP_DATA() {
        return RESP_DATA;
    }

    public void setRESP_DATA(D RESP_DATA) {
        this.RESP_DATA = RESP_DATA;
    }

    @Override
    public String toString() {
        return "PostResponse{" +
                "RSLT_CD='" + RSLT_CD + '\'' +
                ", RSLT_MSG='" + RSLT_MSG + '\'' +
                ", RESP_DATA=" + RESP_DATA +
                '}';
    }
}
