package com.example.bizplay.data.remote.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @SerializedName("USER_ID")
    private String USER_ID;

    @SerializedName("USER_NM")
    private String USER_NM;

    @SerializedName("USER_IMG_PATH")
    private String USER_IMG_PATH;

    @SerializedName("BSNN_NM")
    private String BSNN_NM;

    @SerializedName("BSNN_NO")
    private String BSNN_NO;

    @SerializedName("DVSN_NM")
    private String DVSN_NM;

    @SerializedName("CLPH_NO")
    private String CLPH_NO;

    @SerializedName("EML")
    private String EML;

    @SerializedName("CI_IMG_PATH")
    private String CI_IMG_PATH;

    @SerializedName("CRTC_PATH")
    private String CRTC_PATH;

    @SerializedName("IMG_PATH")
    private String IMG_PATH;

    @SerializedName("FILE_IDNT_ID")
    private String FILE_IDNT_ID;

    @SerializedName("JBCL_NM")
    private String JBCL_NM;

    @SerializedName("RSPT_NM")
    private String RSPT_NM;

    @SerializedName("PPP_APPR_USE_YN")
    private String PPP_APPR_USE_YN;

    @SerializedName("PRVCD_EXCP_YN")
    private String PRVCD_EXCP_YN;

    @SerializedName("BIZ_REC")
    private List<BizResponse> BIZ_REC;

    @SerializedName("PTL_ID")
    private String PTL_ID;

    @SerializedName("USE_INTT_ID")
    private String USE_INTT_ID;

    @SerializedName("CHNL_ID")
    private String CHNL_ID;

    @SerializedName("ERP_APPR_CD")
    private String ERP_APPR_CD;

    @SerializedName("CUST_DSGN_YN")
    private String CUST_DSGN_YN;

    @SerializedName("RCPT_ENTR_EXCP_YN")
    private String RCPT_ENTR_EXCP_YN;

    @SerializedName("RCPT_MAGR_CHG_YN")
    private String RCPT_MAGR_CHG_YN;

    @SerializedName("USE_INTT_REC")
    private List<UseInitResponse> USE_INTT_REC;

    @SerializedName("TRAN_CNT")
    private String TRAN_CNT;

    @SerializedName("NOTI_CNT")
    private String NOTI_CNT;

    @SerializedName("ERP_BGT_YN")
    private String ERP_BGT_YN;

    @SerializedName("OCR_TYPE")
    private String OCR_TYPE;

    @SerializedName("USER_NM_DSPY")
    private String USER_NM_DSPY;

    @SerializedName("CLPH_NO_DSPY")
    private String CLPH_NO_DSPY;

    @SerializedName("EML_DSPY")
    private String EML_DSPY;

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_NM() {
        return USER_NM;
    }

    public void setUSER_NM(String USER_NM) {
        this.USER_NM = USER_NM;
    }

    public String getUSER_IMG_PATH() {
        return USER_IMG_PATH;
    }

    public void setUSER_IMG_PATH(String USER_IMG_PATH) {
        this.USER_IMG_PATH = USER_IMG_PATH;
    }

    public String getBSNN_NM() {
        return BSNN_NM;
    }

    public void setBSNN_NM(String BSNN_NM) {
        this.BSNN_NM = BSNN_NM;
    }

    public String getBSNN_NO() {
        return BSNN_NO;
    }

    public void setBSNN_NO(String BSNN_NO) {
        this.BSNN_NO = BSNN_NO;
    }

    public String getDVSN_NM() {
        return DVSN_NM;
    }

    public void setDVSN_NM(String DVSN_NM) {
        this.DVSN_NM = DVSN_NM;
    }

    public String getCLPH_NO() {
        return CLPH_NO;
    }

    public void setCLPH_NO(String CLPH_NO) {
        this.CLPH_NO = CLPH_NO;
    }

    public String getEML() {
        return EML;
    }

    public void setEML(String EML) {
        this.EML = EML;
    }

    public String getCI_IMG_PATH() {
        return CI_IMG_PATH;
    }

    public void setCI_IMG_PATH(String CI_IMG_PATH) {
        this.CI_IMG_PATH = CI_IMG_PATH;
    }

    public String getCRTC_PATH() {
        return CRTC_PATH;
    }

    public void setCRTC_PATH(String CRTC_PATH) {
        this.CRTC_PATH = CRTC_PATH;
    }

    public String getIMG_PATH() {
        return IMG_PATH;
    }

    public void setIMG_PATH(String IMG_PATH) {
        this.IMG_PATH = IMG_PATH;
    }

    public String getFILE_IDNT_ID() {
        return FILE_IDNT_ID;
    }

    public void setFILE_IDNT_ID(String FILE_IDNT_ID) {
        this.FILE_IDNT_ID = FILE_IDNT_ID;
    }

    public String getJBCL_NM() {
        return JBCL_NM;
    }

    public void setJBCL_NM(String JBCL_NM) {
        this.JBCL_NM = JBCL_NM;
    }

    public String getRSPT_NM() {
        return RSPT_NM;
    }

    public void setRSPT_NM(String RSPT_NM) {
        this.RSPT_NM = RSPT_NM;
    }

    public String getPPP_APPR_USE_YN() {
        return PPP_APPR_USE_YN;
    }

    public void setPPP_APPR_USE_YN(String PPP_APPR_USE_YN) {
        this.PPP_APPR_USE_YN = PPP_APPR_USE_YN;
    }

    public String getPRVCD_EXCP_YN() {
        return PRVCD_EXCP_YN;
    }

    public void setPRVCD_EXCP_YN(String PRVCD_EXCP_YN) {
        this.PRVCD_EXCP_YN = PRVCD_EXCP_YN;
    }

    public List<BizResponse> getBIZ_REC() {
        return BIZ_REC;
    }

    public void setBIZ_REC(List<BizResponse> BIZ_REC) {
        this.BIZ_REC = BIZ_REC;
    }

    public String getPTL_ID() {
        return PTL_ID;
    }

    public void setPTL_ID(String PTL_ID) {
        this.PTL_ID = PTL_ID;
    }

    public String getUSE_INTT_ID() {
        return USE_INTT_ID;
    }

    public void setUSE_INTT_ID(String USE_INTT_ID) {
        this.USE_INTT_ID = USE_INTT_ID;
    }

    public String getCHNL_ID() {
        return CHNL_ID;
    }

    public void setCHNL_ID(String CHNL_ID) {
        this.CHNL_ID = CHNL_ID;
    }

    public String getERP_APPR_CD() {
        return ERP_APPR_CD;
    }

    public void setERP_APPR_CD(String ERP_APPR_CD) {
        this.ERP_APPR_CD = ERP_APPR_CD;
    }

    public String getCUST_DSGN_YN() {
        return CUST_DSGN_YN;
    }

    public void setCUST_DSGN_YN(String CUST_DSGN_YN) {
        this.CUST_DSGN_YN = CUST_DSGN_YN;
    }

    public String getRCPT_ENTR_EXCP_YN() {
        return RCPT_ENTR_EXCP_YN;
    }

    public void setRCPT_ENTR_EXCP_YN(String RCPT_ENTR_EXCP_YN) {
        this.RCPT_ENTR_EXCP_YN = RCPT_ENTR_EXCP_YN;
    }

    public String getRCPT_MAGR_CHG_YN() {
        return RCPT_MAGR_CHG_YN;
    }

    public void setRCPT_MAGR_CHG_YN(String RCPT_MAGR_CHG_YN) {
        this.RCPT_MAGR_CHG_YN = RCPT_MAGR_CHG_YN;
    }

    public List<UseInitResponse> getUSE_INTT_REC() {
        return USE_INTT_REC;
    }

    public void setUSE_INTT_REC(List<UseInitResponse> USE_INTT_REC) {
        this.USE_INTT_REC = USE_INTT_REC;
    }

    public String getTRAN_CNT() {
        return TRAN_CNT;
    }

    public void setTRAN_CNT(String TRAN_CNT) {
        this.TRAN_CNT = TRAN_CNT;
    }

    public String getNOTI_CNT() {
        return NOTI_CNT;
    }

    public void setNOTI_CNT(String NOTI_CNT) {
        this.NOTI_CNT = NOTI_CNT;
    }

    public String getERP_BGT_YN() {
        return ERP_BGT_YN;
    }

    public void setERP_BGT_YN(String ERP_BGT_YN) {
        this.ERP_BGT_YN = ERP_BGT_YN;
    }

    public String getOCR_TYPE() {
        return OCR_TYPE;
    }

    public void setOCR_TYPE(String OCR_TYPE) {
        this.OCR_TYPE = OCR_TYPE;
    }

    public String getUSER_NM_DSPY() {
        return USER_NM_DSPY;
    }

    public void setUSER_NM_DSPY(String USER_NM_DSPY) {
        this.USER_NM_DSPY = USER_NM_DSPY;
    }

    public String getCLPH_NO_DSPY() {
        return CLPH_NO_DSPY;
    }

    public void setCLPH_NO_DSPY(String CLPH_NO_DSPY) {
        this.CLPH_NO_DSPY = CLPH_NO_DSPY;
    }

    public String getEML_DSPY() {
        return EML_DSPY;
    }

    public void setEML_DSPY(String EML_DSPY) {
        this.EML_DSPY = EML_DSPY;
    }


    @Override
    public String toString() {
        return "LoginResponse{" +
                "USER_ID='" + USER_ID + '\'' +
                ", USER_NM='" + USER_NM + '\'' +
                ", USER_IMG_PATH='" + USER_IMG_PATH + '\'' +
                ", BSNN_NM='" + BSNN_NM + '\'' +
                ", BSNN_NO='" + BSNN_NO + '\'' +
                ", DVSN_NM='" + DVSN_NM + '\'' +
                ", CLPH_NO='" + CLPH_NO + '\'' +
                ", EML='" + EML + '\'' +
                ", CI_IMG_PATH='" + CI_IMG_PATH + '\'' +
                ", CRTC_PATH='" + CRTC_PATH + '\'' +
                ", IMG_PATH='" + IMG_PATH + '\'' +
                ", FILE_IDNT_ID='" + FILE_IDNT_ID + '\'' +
                ", JBCL_NM='" + JBCL_NM + '\'' +
                ", RSPT_NM='" + RSPT_NM + '\'' +
                ", PPP_APPR_USE_YN='" + PPP_APPR_USE_YN + '\'' +
                ", PRVCD_EXCP_YN='" + PRVCD_EXCP_YN + '\'' +
                ", BIZ_REC=" + BIZ_REC +
                ", PTL_ID='" + PTL_ID + '\'' +
                ", USE_INTT_ID='" + USE_INTT_ID + '\'' +
                ", CHNL_ID='" + CHNL_ID + '\'' +
                ", ERP_APPR_CD='" + ERP_APPR_CD + '\'' +
                ", CUST_DSGN_YN='" + CUST_DSGN_YN + '\'' +
                ", RCPT_ENTR_EXCP_YN='" + RCPT_ENTR_EXCP_YN + '\'' +
                ", RCPT_MAGR_CHG_YN='" + RCPT_MAGR_CHG_YN + '\'' +
                ", USE_INTT_REC=" + USE_INTT_REC +
                ", TRAN_CNT='" + TRAN_CNT + '\'' +
                ", NOTI_CNT='" + NOTI_CNT + '\'' +
                ", ERP_BGT_YN='" + ERP_BGT_YN + '\'' +
                ", OCR_TYPE='" + OCR_TYPE + '\'' +
                ", USER_NM_DSPY='" + USER_NM_DSPY + '\'' +
                ", CLPH_NO_DSPY='" + CLPH_NO_DSPY + '\'' +
                ", EML_DSPY='" + EML_DSPY + '\'' +
                '}';
    }
}
