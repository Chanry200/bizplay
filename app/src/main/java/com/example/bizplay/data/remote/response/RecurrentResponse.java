package com.example.bizplay.data.remote.response;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class RecurrentResponse implements Cloneable{

    @SerializedName("INQ_GRP")
    private String INQ_GRP;

    @SerializedName("TX_GB")
    private String TX_GB;

    @SerializedName("CARD_CORP_CD")
    private String CARD_CORP_CD;

    @SerializedName("CARD_NO")
    private String CARD_NO;

    @SerializedName("APV_DT")
    private String APV_DT;

    @SerializedName("APV_SEQ")
    private String APV_SEQ;

    @SerializedName("APV_TM")
    private String APV_TM;

    @SerializedName("APV_NO")
    private String APV_NO;

    @SerializedName("APV_AMT")
    private String APV_AMT;

    @SerializedName("APV_CAN_YN")
    private String APV_CAN_YN;

    @SerializedName("APV_CAN_DT")
    private String APV_CAN_DT;

    @SerializedName("ITLM_MMS_CNT")
    private String ITLM_MMS_CNT;

    @SerializedName("SETL_SCHE_DT")
    private String SETL_SCHE_DT;

    @SerializedName("MEST_NM")
    private String MEST_NM;

    @SerializedName("MEST_NO")
    private String MEST_NO;

    @SerializedName("MEST_BIZ_NO")
    private String MEST_BIZ_NO;

    @SerializedName("RCPT_TX_STS")
    private String RCPT_TX_STS;

    @SerializedName("ETC_REG_YN")
    private String ETC_REG_YN;

    @SerializedName("ETC_IMG_YN")
    private String ETC_IMG_YN;

    @SerializedName("ETC_RCPT_IMG_URL")
    private String ETC_RCPT_IMG_URL;

    @SerializedName("CURR_CD")
    private String CURR_CD;

    @SerializedName("OVRS_TX_AMT ")
    private String OVRS_TX_AMT ;

    @SerializedName("RCPT_RFS_STS")
    private String RCPT_RFS_STS;

    @SerializedName("CARD_ORG_NM")
    private String CARD_ORG_NM;

    @SerializedName("CARD_NICK_NM")
    private String CARD_NICK_NM;

    @SerializedName("DAY_NM")
    private String DAY_NM;

    @SerializedName("TRAN_KIND_CD")
    private String TRAN_KIND_CD;

    @SerializedName("TRAN_KIND_NM")
    private String TRAN_KIND_NM;

    @SerializedName("PPP_APPR_STS")
    private String PPP_APPR_STS;

    @SerializedName("PPP_APPR_GB")
    private String PPP_APPR_GB;

    @SerializedName("APPR_SEQ_NO")
    private String APPR_SEQ_NO;

    @SerializedName("DOC_NO")
    private String DOC_NO;

    @SerializedName("IMG_YN")
    private String IMG_YN;

    @SerializedName("CLS_YN")
    private String CLS_YN;

    @SerializedName("OCR_YN")
    private String OCR_YN;

    @SerializedName("OCR_TYPE")
    private String OCR_TYPE;

    private boolean isChecked;

    public String getINQ_GRP() {
        return INQ_GRP;
    }

    public void setINQ_GRP(String INQ_GRP) {
        this.INQ_GRP = INQ_GRP;
    }

    public String getTX_GB() {
        return TX_GB;
    }

    public void setTX_GB(String TX_GB) {
        this.TX_GB = TX_GB;
    }

    public String getCARD_CORP_CD() {
        return CARD_CORP_CD;
    }

    public void setCARD_CORP_CD(String CARD_CORP_CD) {
        this.CARD_CORP_CD = CARD_CORP_CD;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public String getAPV_DT() {
        return APV_DT;
    }

    public void setAPV_DT(String APV_DT) {
        this.APV_DT = APV_DT;
    }

    public String getAPV_SEQ() {
        return APV_SEQ;
    }

    public void setAPV_SEQ(String APV_SEQ) {
        this.APV_SEQ = APV_SEQ;
    }

    public String getAPV_TM() {
        return APV_TM;
    }

    public void setAPV_TM(String APV_TM) {
        this.APV_TM = APV_TM;
    }

    public String getAPV_NO() {
        return APV_NO;
    }

    public void setAPV_NO(String APV_NO) {
        this.APV_NO = APV_NO;
    }

    public String getAPV_AMT() {
        return APV_AMT;
    }

    public void setAPV_AMT(String APV_AMT) {
        this.APV_AMT = APV_AMT;
    }

    public String getAPV_CAN_YN() {
        return APV_CAN_YN;
    }

    public void setAPV_CAN_YN(String APV_CAN_YN) {
        this.APV_CAN_YN = APV_CAN_YN;
    }

    public String getAPV_CAN_DT() {
        return APV_CAN_DT;
    }

    public void setAPV_CAN_DT(String APV_CAN_DT) {
        this.APV_CAN_DT = APV_CAN_DT;
    }

    public String getITLM_MMS_CNT() {
        return ITLM_MMS_CNT;
    }

    public void setITLM_MMS_CNT(String ITLM_MMS_CNT) {
        this.ITLM_MMS_CNT = ITLM_MMS_CNT;
    }

    public String getSETL_SCHE_DT() {
        return SETL_SCHE_DT;
    }

    public void setSETL_SCHE_DT(String SETL_SCHE_DT) {
        this.SETL_SCHE_DT = SETL_SCHE_DT;
    }

    public String getMEST_NM() {
        return MEST_NM;
    }

    public void setMEST_NM(String MEST_NM) {
        this.MEST_NM = MEST_NM;
    }

    public String getMEST_NO() {
        return MEST_NO;
    }

    public void setMEST_NO(String MEST_NO) {
        this.MEST_NO = MEST_NO;
    }

    public String getMEST_BIZ_NO() {
        return MEST_BIZ_NO;
    }

    public void setMEST_BIZ_NO(String MEST_BIZ_NO) {
        this.MEST_BIZ_NO = MEST_BIZ_NO;
    }

    public String getRCPT_TX_STS() {
        return RCPT_TX_STS;
    }

    public void setRCPT_TX_STS(String RCPT_TX_STS) {
        this.RCPT_TX_STS = RCPT_TX_STS;
    }

    public String getETC_REG_YN() {
        return ETC_REG_YN;
    }

    public void setETC_REG_YN(String ETC_REG_YN) {
        this.ETC_REG_YN = ETC_REG_YN;
    }

    public String getETC_IMG_YN() {
        return ETC_IMG_YN;
    }

    public void setETC_IMG_YN(String ETC_IMG_YN) {
        this.ETC_IMG_YN = ETC_IMG_YN;
    }

    public String getETC_RCPT_IMG_URL() {
        return ETC_RCPT_IMG_URL;
    }

    public void setETC_RCPT_IMG_URL(String ETC_RCPT_IMG_URL) {
        this.ETC_RCPT_IMG_URL = ETC_RCPT_IMG_URL;
    }

    public String getCURR_CD() {
        return CURR_CD;
    }

    public void setCURR_CD(String CURR_CD) {
        this.CURR_CD = CURR_CD;
    }

    public String getOVRS_TX_AMT() {
        return OVRS_TX_AMT;
    }

    public void setOVRS_TX_AMT(String OVRS_TX_AMT) {
        this.OVRS_TX_AMT = OVRS_TX_AMT;
    }

    public String getRCPT_RFS_STS() {
        return RCPT_RFS_STS;
    }

    public void setRCPT_RFS_STS(String RCPT_RFS_STS) {
        this.RCPT_RFS_STS = RCPT_RFS_STS;
    }

    public String getCARD_ORG_NM() {
        return CARD_ORG_NM;
    }

    public void setCARD_ORG_NM(String CARD_ORG_NM) {
        this.CARD_ORG_NM = CARD_ORG_NM;
    }

    public String getCARD_NICK_NM() {
        return CARD_NICK_NM;
    }

    public void setCARD_NICK_NM(String CARD_NICK_NM) {
        this.CARD_NICK_NM = CARD_NICK_NM;
    }

    public String getDAY_NM() {
        return DAY_NM;
    }

    public void setDAY_NM(String DAY_NM) {
        this.DAY_NM = DAY_NM;
    }

    public String getTRAN_KIND_CD() {
        return TRAN_KIND_CD;
    }

    public void setTRAN_KIND_CD(String TRAN_KIND_CD) {
        this.TRAN_KIND_CD = TRAN_KIND_CD;
    }

    public String getTRAN_KIND_NM() {
        return TRAN_KIND_NM;
    }

    public void setTRAN_KIND_NM(String TRAN_KIND_NM) {
        this.TRAN_KIND_NM = TRAN_KIND_NM;
    }

    public String getPPP_APPR_STS() {
        return PPP_APPR_STS;
    }

    public void setPPP_APPR_STS(String PPP_APPR_STS) {
        this.PPP_APPR_STS = PPP_APPR_STS;
    }

    public String getPPP_APPR_GB() {
        return PPP_APPR_GB;
    }

    public void setPPP_APPR_GB(String PPP_APPR_GB) {
        this.PPP_APPR_GB = PPP_APPR_GB;
    }

    public String getAPPR_SEQ_NO() {
        return APPR_SEQ_NO;
    }

    public void setAPPR_SEQ_NO(String APPR_SEQ_NO) {
        this.APPR_SEQ_NO = APPR_SEQ_NO;
    }

    public String getDOC_NO() {
        return DOC_NO;
    }

    public void setDOC_NO(String DOC_NO) {
        this.DOC_NO = DOC_NO;
    }

    public String getIMG_YN() {
        return IMG_YN;
    }

    public void setIMG_YN(String IMG_YN) {
        this.IMG_YN = IMG_YN;
    }

    public String getCLS_YN() {
        return CLS_YN;
    }

    public void setCLS_YN(String CLS_YN) {
        this.CLS_YN = CLS_YN;
    }

    public String getOCR_YN() {
        return OCR_YN;
    }

    public void setOCR_YN(String OCR_YN) {
        this.OCR_YN = OCR_YN;
    }

    public String getOCR_TYPE() {
        return OCR_TYPE;
    }

    public void setOCR_TYPE(String OCR_TYPE) {
        this.OCR_TYPE = OCR_TYPE;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString() {
        return "CurrentPortionReceiptRespone{" +
                "INQ_GRP='" + INQ_GRP + '\'' +
                ", TX_GB='" + TX_GB + '\'' +
                ", CARD_CORP_CD='" + CARD_CORP_CD + '\'' +
                ", CARD_NO='" + CARD_NO + '\'' +
                ", APV_DT='" + APV_DT + '\'' +
                ", APV_SEQ='" + APV_SEQ + '\'' +
                ", APV_TM='" + APV_TM + '\'' +
                ", APV_NO='" + APV_NO + '\'' +
                ", APV_AMT='" + APV_AMT + '\'' +
                ", APV_CAN_YN='" + APV_CAN_YN + '\'' +
                ", APV_CAN_DT='" + APV_CAN_DT + '\'' +
                ", ITLM_MMS_CNT='" + ITLM_MMS_CNT + '\'' +
                ", SETL_SCHE_DT='" + SETL_SCHE_DT + '\'' +
                ", MEST_NM='" + MEST_NM + '\'' +
                ", MEST_NO='" + MEST_NO + '\'' +
                ", MEST_BIZ_NO='" + MEST_BIZ_NO + '\'' +
                ", RCPT_TX_STS='" + RCPT_TX_STS + '\'' +
                ", ETC_REG_YN='" + ETC_REG_YN + '\'' +
                ", ETC_IMG_YN='" + ETC_IMG_YN + '\'' +
                ", ETC_RCPT_IMG_URL='" + ETC_RCPT_IMG_URL + '\'' +
                ", CURR_CD='" + CURR_CD + '\'' +
                ", OVRS_TX_AMT='" + OVRS_TX_AMT + '\'' +
                ", RCPT_RFS_STS='" + RCPT_RFS_STS + '\'' +
                ", CARD_ORG_NM='" + CARD_ORG_NM + '\'' +
                ", CARD_NICK_NM='" + CARD_NICK_NM + '\'' +
                ", DAY_NM='" + DAY_NM + '\'' +
                ", TRAN_KIND_CD='" + TRAN_KIND_CD + '\'' +
                ", TRAN_KIND_NM='" + TRAN_KIND_NM + '\'' +
                ", PPP_APPR_STS='" + PPP_APPR_STS + '\'' +
                ", PPP_APPR_GB='" + PPP_APPR_GB + '\'' +
                ", APPR_SEQ_NO='" + APPR_SEQ_NO + '\'' +
                ", DOC_NO='" + DOC_NO + '\'' +
                ", IMG_YN='" + IMG_YN + '\'' +
                ", CLS_YN='" + CLS_YN + '\'' +
                ", OCR_YN='" + OCR_YN + '\'' +
                ", OCR_TYPE='" + OCR_TYPE + '\'' +
                '}';
    }

    @NonNull
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
