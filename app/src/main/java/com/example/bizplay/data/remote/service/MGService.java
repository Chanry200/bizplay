package com.example.bizplay.data.remote.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MGService {
    @GET("https://mg-dev.bizplay.co.kr/MgGate?master_id=A_NEW_BIPLE_G_1")
    Call<String> getAllMG();
}
