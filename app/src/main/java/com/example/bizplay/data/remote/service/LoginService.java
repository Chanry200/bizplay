package com.example.bizplay.data.remote.service;

import com.example.bizplay.data.remote.response.LoginResponse;
import com.example.bizplay.data.remote.response.PostResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface LoginService {

    @Headers({
            "Content-Type: application/x-www-form-urlencoded; charset=utf-8",
            "charset: UTF-8",
            "Content-Length: 610",
            "User-Agent:  null;nma-plf=ADR;nma-plf-ver=30;nma-plf-ver_os=11nma-app-id=com.bizcard.bizplay;nma-model=sdk_gphone_x86;nma-app-ver=4.2.51;nma-dev-id=4b52002e-a477-4bd5-92ad-dc73a30c8205",
//            "Cookie: JSESSIONID=C10FAE4F057609AA48F9078EC5ABCBEA"
    })
    @POST("https://webank.appplay.co.kr/CardAPI.do")
    Call<PostResponse<LoginResponse>> createLogin (@Body RequestBody requestBody);

}
