package com.example.bizplay.data.repository;

import androidx.lifecycle.MutableLiveData;

public interface MGRepository {
    MutableLiveData<String> getMG();
}
