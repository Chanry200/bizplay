package com.example.bizplay.data.remote.response;

import com.google.gson.annotations.SerializedName;

public class BizResponse {
    @SerializedName("BIZ_NO")
    private String BIZ_NO;

    @SerializedName("BIZ_NM")
    private String BIZ_NM;

    public BizResponse(String BIZ_NO, String BIZ_NM) {
        this.BIZ_NO = BIZ_NO;
        this.BIZ_NM = BIZ_NM;
    }

    public String getBIZ_NO() {
        return BIZ_NO;
    }

    public void setBIZ_NO(String BIZ_NO) {
        this.BIZ_NO = BIZ_NO;
    }

    public String getBIZ_NM() {
        return BIZ_NM;
    }

    public void setBIZ_NM(String BIZ_NM) {
        this.BIZ_NM = BIZ_NM;
    }

    @Override
    public String toString() {
        return "bizResponse{" +
                "BIZ_NO='" + BIZ_NO + '\'' +
                ", BIZ_NM='" + BIZ_NM + '\'' +
                '}';
    }
}
