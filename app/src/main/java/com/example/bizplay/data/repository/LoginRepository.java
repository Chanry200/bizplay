package com.example.bizplay.data.repository;

import androidx.lifecycle.MutableLiveData;

import com.example.bizplay.data.remote.response.LoginResponse;
import com.example.bizplay.data.remote.response.PostResponse;

public interface LoginRepository {
    MutableLiveData<PostResponse<LoginResponse>> login(String login);

}
