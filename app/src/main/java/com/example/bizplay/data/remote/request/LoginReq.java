package com.example.bizplay.data.remote.request;

import org.json.JSONException;
import org.json.JSONObject;

//TODO
public class  LoginReq extends JSONObject {

    public void setUSE_INTT_ID(String USE_INTT_ID) throws JSONException {
        put("USE_INTT_ID",USE_INTT_ID);
    }

    public void setUSER_ID (String USER_ID) throws JSONException{
        put("USER_ID",USER_ID);
    }

    public void setENC_GB(String ENC_GB) throws JSONException {
        put("ENC_GB",ENC_GB);
    }

    public void setUSER_PW(String USER_PW) throws JSONException{
        put("USER_PW",USER_PW);
    }


    public void setMOBL_CD(String MOBL_CD) throws JSONException {
        put("MOBL_CD",MOBL_CD);
    }

    public void setSESSION_ID(String SESSION_ID) throws JSONException {
        put("SESSION_ID",SESSION_ID);
    }

    public void setTOKEN(String TOKEN) throws JSONException {
        put("TOKEN",TOKEN);
    }

    public void setNATION_CD(String NATION_CD) throws JSONException {
        put("NATION_CD",NATION_CD);
    }



}
