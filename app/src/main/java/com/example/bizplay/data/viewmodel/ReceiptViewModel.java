package com.example.bizplay.data.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.bizplay.data.remote.response.PostResponse;
import com.example.bizplay.data.remote.response.ReceiptResponse;
import com.example.bizplay.repositoryImp.ReceiptImp;

public class ReceiptViewModel extends AndroidViewModel {
    ReceiptImp receiptImp;

    public ReceiptViewModel(@NonNull Application application) {
        super(application);
        receiptImp = ReceiptImp.newInstance();
    }

    public LiveData<PostResponse<ReceiptResponse>> getReceipt (String receipt){
        return receiptImp.getReceipt(receipt);
    }
}
