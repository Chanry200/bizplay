package com.example.bizplay.data.remote.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceiptResponse {

    @SerializedName("INQ_NEXT_CARD_NO")
    private String INQ_NEXT_CARD_NO;

    @SerializedName("INQ_NEXT_APV_DT")
    private String INQ_NEXT_APV_DT;

    @SerializedName("INQ_NEXT_APV_SEQ")
    private String INQ_NEXT_APV_SEQ;

    @SerializedName("INQ_NEXT_APV_TM")
    private String INQ_NEXT_APV_TM;

    @SerializedName("INQ_NEXT_APV_NO")
    private String INQ_NEXT_APV_NO;

    @SerializedName("INQ_ITRN_SUB_ROWCOUNT")
    private String INQ_ITRN_SUB_ROWCOUNT;

    @SerializedName("INQ_ITRN_SUB")
    private List<RecurrentResponse> INQ_ITRN_SUB;

    @SerializedName("TRAN_CNT")
    private String TRAN_CNT;

    @SerializedName("NOTI_CNT")
    private String NOTI_CNT;

    @SerializedName("CLS_RCPT_RSTRCT_YN")
    private String CLS_RCPT_RSTRCT_YN;

    @SerializedName("CLS_APPR_RSTRCT_YN")
    private String CLS_APPR_RSTRCT_YN;

    @SerializedName("RCPT_CLS_MSG")
    private String RCPT_CLS_MSG;

    @SerializedName("HPS_RCPT_CNT")
    private String HPS_RCPT_CNT;

    public String getINQ_NEXT_CARD_NO() {
        return INQ_NEXT_CARD_NO;
    }

    public void setINQ_NEXT_CARD_NO(String INQ_NEXT_CARD_NO) {
        this.INQ_NEXT_CARD_NO = INQ_NEXT_CARD_NO;
    }

    public String getINQ_NEXT_APV_DT() {
        return INQ_NEXT_APV_DT;
    }

    public void setINQ_NEXT_APV_DT(String INQ_NEXT_APV_DT) {
        this.INQ_NEXT_APV_DT = INQ_NEXT_APV_DT;
    }

    public String getINQ_NEXT_APV_SEQ() {
        return INQ_NEXT_APV_SEQ;
    }

    public void setINQ_NEXT_APV_SEQ(String INQ_NEXT_APV_SEQ) {
        this.INQ_NEXT_APV_SEQ = INQ_NEXT_APV_SEQ;
    }

    public String getINQ_NEXT_APV_TM() {
        return INQ_NEXT_APV_TM;
    }

    public void setINQ_NEXT_APV_TM(String INQ_NEXT_APV_TM) {
        this.INQ_NEXT_APV_TM = INQ_NEXT_APV_TM;
    }

    public String getINQ_NEXT_APV_NO() {
        return INQ_NEXT_APV_NO;
    }

    public void setINQ_NEXT_APV_NO(String INQ_NEXT_APV_NO) {
        this.INQ_NEXT_APV_NO = INQ_NEXT_APV_NO;
    }

    public String getINQ_ITRN_SUB_ROWCOUNT() {
        return INQ_ITRN_SUB_ROWCOUNT;
    }

    public void setINQ_ITRN_SUB_ROWCOUNT(String INQ_ITRN_SUB_ROWCOUNT) {
        this.INQ_ITRN_SUB_ROWCOUNT = INQ_ITRN_SUB_ROWCOUNT;
    }

    public List<RecurrentResponse> getINQ_ITRN_SUB() {
        return INQ_ITRN_SUB;
    }

    public void setINQ_ITRN_SUB(List<RecurrentResponse> INQ_ITRN_SUB) {
        this.INQ_ITRN_SUB = INQ_ITRN_SUB;
    }

    public String getTRAN_CNT() {
        return TRAN_CNT;
    }

    public void setTRAN_CNT(String TRAN_CNT) {
        this.TRAN_CNT = TRAN_CNT;
    }

    public String getNOTI_CNT() {
        return NOTI_CNT;
    }

    public void setNOTI_CNT(String NOTI_CNT) {
        this.NOTI_CNT = NOTI_CNT;
    }

    public String getCLS_RCPT_RSTRCT_YN() {
        return CLS_RCPT_RSTRCT_YN;
    }

    public void setCLS_RCPT_RSTRCT_YN(String CLS_RCPT_RSTRCT_YN) {
        this.CLS_RCPT_RSTRCT_YN = CLS_RCPT_RSTRCT_YN;
    }

    public String getCLS_APPR_RSTRCT_YN() {
        return CLS_APPR_RSTRCT_YN;
    }

    public void setCLS_APPR_RSTRCT_YN(String CLS_APPR_RSTRCT_YN) {
        this.CLS_APPR_RSTRCT_YN = CLS_APPR_RSTRCT_YN;
    }

    public String getRCPT_CLS_MSG() {
        return RCPT_CLS_MSG;
    }

    public void setRCPT_CLS_MSG(String RCPT_CLS_MSG) {
        this.RCPT_CLS_MSG = RCPT_CLS_MSG;
    }

    public String getHPS_RCPT_CNT() {
        return HPS_RCPT_CNT;
    }

    public void setHPS_RCPT_CNT(String HPS_RCPT_CNT) {
        this.HPS_RCPT_CNT = HPS_RCPT_CNT;
    }

    @Override
    public String toString() {
        return "ReceiptResponse{" +
                "INQ_NEXT_CARD_NO='" + INQ_NEXT_CARD_NO + '\'' +
                ", INQ_NEXT_APV_DT='" + INQ_NEXT_APV_DT + '\'' +
                ", INQ_NEXT_APV_SEQ='" + INQ_NEXT_APV_SEQ + '\'' +
                ", INQ_NEXT_APV_TM='" + INQ_NEXT_APV_TM + '\'' +
                ", INQ_NEXT_APV_NO='" + INQ_NEXT_APV_NO + '\'' +
                ", INQ_ITRN_SUB_ROWCOUNT='" + INQ_ITRN_SUB_ROWCOUNT + '\'' +
                ", INQ_ITRN_SUB=" + INQ_ITRN_SUB +
                ", TRAN_CNT='" + TRAN_CNT + '\'' +
                ", NOTI_CNT='" + NOTI_CNT + '\'' +
                ", CLS_RCPT_RSTRCT_YN='" + CLS_RCPT_RSTRCT_YN + '\'' +
                ", CLS_APPR_RSTRCT_YN='" + CLS_APPR_RSTRCT_YN + '\'' +
                ", RCPT_CLS_MSG='" + RCPT_CLS_MSG + '\'' +
                ", HPS_RCPT_CNT='" + HPS_RCPT_CNT + '\'' +
                '}';
    }
}
