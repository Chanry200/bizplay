package com.example.bizplay.utils;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.webkit.JavascriptInterface;

import androidx.annotation.NonNull;

import org.json.JSONObject;

public class BrowserBridge {
    private final android.os.Handler mHandler;
    private final Activity mActivity;
    private OnActionCode onActionCode;

    public BrowserBridge(@NonNull Activity activity) {
        mHandler = new android.os.Handler();
        mActivity = activity;
    }

    public BrowserBridge(@NonNull Activity activity, OnActionCode onActionCode) {
        mHandler = new Handler();
        mActivity = activity;
        this.onActionCode = onActionCode;
    }

    //WebView Processing
    @JavascriptInterface
    public void iWebAction(final String jsondata) {
        mHandler.post(() -> {
            JSONObject jsonObject;
            try {

                Log.d("iWebAction", "iWebAction::: " + jsondata);
                if (mActivity.isFinishing()) {
                    return;
                }

                jsonObject = new JSONObject(jsondata);
                String actionCode = jsonObject.getString("_action_code");

                if (onActionCode != null) {
                    onActionCode.actionCode(actionCode, jsonObject);
                }

                switch (actionCode) {
                    case "4000":
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public interface OnActionCode {
        void actionCode(String code, JSONObject jsonObject);
    }

}
