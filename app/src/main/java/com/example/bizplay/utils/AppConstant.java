package com.example.bizplay.utils;

public class AppConstant {
    public static final String MG = "MG" ;
    public static final String BPCD_MBL_P001 = "BPCD_MBL_P001";
    public static final String MY_LOGIN = "MY_LOGIN";
    public static final String USER_INTT_ID = "userINTTId";
    public static final String BODY_LOGIN = "bodyLogin";
    public static final String NAME_DISPLAY = "%s";

    public static final String BPCD_MBL_L_002 = "BPCD_MBL_L002";
}
