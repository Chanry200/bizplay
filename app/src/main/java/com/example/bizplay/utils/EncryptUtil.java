package com.example.bizplay.utils;

import java.util.Locale;

public class EncryptUtil {
    public static String encode(String t){
        if(t == null || t.length() == 0){
            return "";
        }
        return byteArrayToHex(XorEnc(t.getBytes()));
    }

    private static byte[] XorEnc(byte[] b){
        int l = b.length;
        for( int i = 0; i < l; i++) {
            b[i] = ( byte ) (b[i] ^ 0x9f);
            b[i] = ( byte ) ((b[i] & 0xff) >> 4 | (b[i] & 0x0f) << 4);
        }
        return b;
    }

    private static String byteArrayToHex(byte[] a) {
        if (a == null || a.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b).toUpperCase(Locale.ROOT));
        return sb.toString();
    }
}
